<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Redirect;
use Session;
session_start();

class AdminController extends Controller
{
	public function __construct() 
	{
		//$this->middleware("CheckSession");
	}
    
	
	public function index()
    {
		$data['action'] ='/admin-login-check';
		return view('admin.admin_login',$data);
    }
	
    public function adminLoginCheck(Request $request) 
    {
		
		  
		
		$email_address = $request->input('email_address');
        $admin_password = md5($request->input('admin_password'));

        $admin_info = DB::table('tbl_admin as ad')                    
					 ->leftJoin('tbl_admin_user_role as aur', 'ad.access_label', '=', 'aur.id')
					 ->leftJoin('tbl_ogranization as og', 'ad.org_code', '=', 'og.org_code')
					 ->leftJoin('tbl_branch as b', 'ad.branch_code', '=', 'b.br_code')
					 ->select('ad.*','aur.admin_role_name','b.area_code','b.zone_code','og.org_short_name','og.org_logo','og.favicon','aur.id as role_id')
                     ->where('ad.email_address',$email_address)
                     ->where('ad.admin_password',$admin_password)
                     ->where('ad.status',1)
                     ->first();
        
     

	   if($admin_info)
        {
            Session::put('admin_id',$admin_info->admin_id);
			Session::put('branch_code',$admin_info->branch_code);
			Session::put('emp_id',$admin_info->emp_id);
			Session::put('emp_type',$admin_info->emp_type);
            Session::put('admin_name',$admin_info->admin_name);
            Session::put('admin_photo','public/avatars/'.$admin_info->admin_photo);
            Session::put('admin_access_label',$admin_info->access_label);
            Session::put('admin_role_name',$admin_info->admin_role_name);
            Session::put('user_type',$admin_info->user_type);
            Session::put('area_code',$admin_info->area_code);
            Session::put('zone_code',$admin_info->zone_code);
            Session::put('admin_org_code',$admin_info->org_code);
            Session::put('org_short_name',$admin_info->org_short_name);
            Session::put('org_logo','public/org_logo/'.$admin_info->org_logo);
            Session::put('favicon','public/org_logo/'.$admin_info->favicon);
            if($admin_info->role_id == 12)
			{
				return Redirect::to('/profile');
			}
			else
			{
				return Redirect::to('/dashboard');
			}
        }
        else{
            Session::put('exception','User Id Or Password Invalid !');
            return Redirect::to('/admin');
        }

    }
	
	public function abc()
	{
		$br 			= 9999;
		$date 			= '2020-03-12';
		$your_url 		= 'http://45.114.85.154/hrm/branch-staff/'.$br.'/'.$date;
		$hr_data		= file_get_contents($your_url);
		$my_staffs 		= json_decode($hr_data);

		foreach($my_staffs as $my_staff) { 
			
			$str 	= $my_staff->emp_name_eng;
			$people = (explode(" ",$str));
			$data['branch_code'] 	= $my_staff->br_code;
			$data['emp_id'] 		= $my_staff->emp_id;
			$data['emp_type'] 		= 1;
			$data['first_name'] 	= current($people) ;
			$data['last_name'] 		= end($people);
			$data['admin_name'] 	= $my_staff->emp_name_eng;
			$data['email_address'] 	= $my_staff->emp_id;
			$data['cell_no'] 		= '01313';
			$data['admin_password'] = md5('12345678');
			$data['admin_photo'] 	= $my_staff->emp_id.'.jpg';
			$data['access_label'] 	= 12;
			$data['status'] 		= 1;
			$data['user_type'] 		= 6;
			$data['org_code'] 		= 181;
		}
		
		
		print_r($data);
		exit;

	}


	

}
