<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class SuperadminController extends Controller
{
    public function __construct() 
	{
		//$this->middleware("CheckUserSession");
	}
	
	public function index()
    {
		$access_label = Session::get('admin_access_label');
		$data = array();
		
		$to_days_date = date('Y-m-d');
				
		$data['total_employee'] = DB::table('cdip_hrm.tbl_emp_basic_info as e')
								->leftjoin('cdip_hrm.tbl_resignation as r', 'e.emp_id', '=', 'r.emp_id')
								->where('e.emp_type', '=', 'regular')
								->where('e.org_join_date', '<=', $to_days_date)
								->whereNull('r.emp_id')
								->orWhere('r.effect_date', '>', $to_days_date)
								->count();

		$data['total_resigned_employee'] = DB::table('cdip_hrm.tbl_resignation as r')
								->where('r.effect_date', '<=', $to_days_date)
								->count();
		
		$data['todays_new_emp'] = DB::table('cdip_hrm.tbl_appointment_info as a')
								->where('a.letter_date',$to_days_date)
								->where('a.status',1)
								->count();
		
		$data['todays_resign_emp'] = DB::table('cdip_hrm.tbl_resignation as r')
								->where('r.letter_date',$to_days_date)
								->where('r.status',1)
								->count();
		
		/* file transfer info start */
		$login_emp_id = Session::get('emp_id');
		$data['all_result'] = DB::table('cdip_hrm.tbl_fp_file_info as fi')
									->where('fi.receiver_emp_id', $login_emp_id)
									->where('fi.status',0)
									->get();
		//print_r ($data['all_result']);
		/* file transfer info end */		
		
		$admin_id = Session::get('admin_id');
		$admin_password = md5(123456);
		$admin_info = DB::table('tbl_admin') 
                     ->where('admin_id',$admin_id)
                     ->where('admin_password',$admin_password)
					 ->select('*')
                     ->first(); 
		if($admin_info){
			Session::put('menu_permission',2); // 2 for NO
		}else{
			Session::put('menu_permission',1); // 1 for YES
		}		
		return view('admin.pages.dashboard',$data);
    }
	
    public function logout()
    {
        Session::put('admin_id','');
		Session::put('emp_id','');
        Session::put('admin_name','');
        Session::put('admin_photo','');
        Session::put('admin_access_label','');
        Session::put('admin_role_name','');
        Session::put('admin_org_code','');
        Session::put('org_short_name','');
        Session::put('org_logo','');
        Session::put('favicon','');
        Session::put('message','You are successfully Logout !');
        return Redirect::to('/admin');
    }
	
	
}
