<?php

namespace App\Http\Controllers;

use App\Models\Programforms;
use Illuminate\Http\Request;

class ProgramFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.audit.program_form');
    } 
	public function audit_submission_form(Request $request)
    {
        return view('admin.audit.program_form');
    }
	

    /* ক্রমিন নং ৩  সকল সদস্যদের পাশবই যাচাই সংক্রান্ত */

    public function pass_book_check()
    {
        return view('admin.audit.pass_book_check');
    }



     /* ক্রমিন নং 4  সঞ্চয় ও ঋন সংক্রান্ত */

     public function deposit_and_loan()
     {
         return view('admin.audit.deposit_and_loan');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Programforms  $programforms
     * @return \Illuminate\Http\Response
     */
    public function show(Programforms $programforms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Programforms  $programforms
     * @return \Illuminate\Http\Response
     */
    public function edit(Programforms $programforms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Programforms  $programforms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programforms $programforms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Programforms  $programforms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Programforms $programforms)
    {
        //
    }
}
