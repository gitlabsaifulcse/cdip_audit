@extends('admin.admin_master')
@section('title', 'Dashboard')
@section('main_content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Dashboard<small>Control panel</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<!-- Small boxes (Stat box) -->
		<?php $admin_access_label 			= Session::get('admin_access_label'); 
		if($admin_access_label != 23){
		?>
		<div class="row">
			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{$total_employee}}</h3>
						<p>Total Regular Running Employee</p>
					</div>
					<div class="icon">
						<i class="ion ion-person"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>{{$total_resigned_employee}}</h3>
						<p>Total Resigned Employee</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{$todays_new_emp}}</h3>
						<p>Todays New Employee</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			  <!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>{{$todays_resign_emp}}</h3>
						<p>Todays Resigned Employee</p>
					</div>
					<div class="icon">
						<i class="ion ion-person"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
			</div>
			<!-- ./col -->
		</div>
		@if(count($all_result)>0)
		<div class="row">
			<div class="col-md-6 col-md-offset-3" style="margin-bottom:5px;">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sl No</th>
								<th>Employee ID</th>
								<th>Sender Employee</th>
								<th>Receiver Employee</th>
								<th>Entry Date</th>                
								<th>Status</th>
							</tr>
						</thead>
						<tbody>								
							{{!$i=1}} @foreach($all_result as $result)
							<tr>
								<td>{{$i++}}</td>
								<td>{{$result->fp_emp_id}}</td>
								<td>{{$result->sender_emp_id}}</td>
								<td>{{$result->receiver_emp_id}}</td>
								<td>{{$result->entry_date}}</td>
								<td class="text-center">
									<a class="btn btn-danger" href="{{URL::to('/fp_file_info')}}" >Receive</a>
								</td>
							</tr>
							@endforeach
						</tbody>    
					</table>
				</div>
			</div>
		</div>
		@endif
		<?php } ?>
		<!-- /.row -->
		<!-- Main row -->
		<div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">


        </section>
        <!-- /.Left col -->
        
		</div>
		<!-- /.row (main row) -->

    </section>
	<!-- /.content -->
	<script>
		$(document).ready(function() {
			$("#dashboard").addClass('active');
			//$("#Branch_Transfer").addClass('active');
		});
	</script>
@endsection	