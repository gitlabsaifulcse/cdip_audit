@extends('admin.admin_master')
@section('title', 'Program Form')
@section('main_content')

	<style>
        table, tr, th, td {
        border: 1px  solid black;
		text-align:center;
		font-size:10px !important;
        }
        table{
            width:100%;
        }
		
		* {
		  box-sizing: border-box;
		}

		body {
		  background-color: #f1f1f1;
		}

		.programForm {
		  background-color: #ffffff;
		  font-family: Raleway;
		  width: 100%;
		}

		h1 {
		  text-align: center;  
		}



		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		  padding: 10px;
		}

		button {
		  background-color: #4CAF50;
		  color: #ffffff;
		  border: none;
		  padding: 10px 20px;
		  font-size: 17px;
		  font-family: Raleway;
		  cursor: pointer;
		}

		button:hover {
		  opacity: 0.8;
		}

		#prevBtn {
		  background-color: #bbbbbb;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none;  
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
		.bottom_border {
		  border: 0;
		  outline: 0;
		  background: transparent;
		  border-bottom: 1px solid black;
		}
    </style>

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Program <small>Form</small></h1>
		
	</section>

	<!-- Main content -->

	<section class="content">
	<div class="row">
			<div class="col-12 col-sm-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Program Form</h3>
					</div>
					<div class="box-body">
				

			<!-- Form 1 start ############################################################-->
			
			<div class="tab"> Form 1

				<form class="programForm" action="{{URL::to('audit_submission_form')}}" role="form" method="POST" enctype="multipart/form-data">
						<div class="table-responsive">
							<table>
								<tbody>
									<tr>
										<th>বিষয়  : </th>
										<td colspan="2"><input type="text" class="bottom_border" name="" id=""></td>
										<th> এরিয়ার  : </th>
										<td>
											<input type="text" class="bottom_border" name="" id="">
										</td>
										<th>ব্রাঞ্চের : </th>
										<td>
											<input type="text" class="bottom_border" name="" id="">
										</td>
										<td>/</td>
										<td><input type="text" class="bottom_border" name="" id=""></td>
										<td colspan="2">অডিট প্রতিবেদন প্রেরন প্রসংগে।</td> 
									</tr>
									
									<tr>
										<th>অডিট সপ্তাহ  : </th>
										<td><input type="date" class="bottom_border" name="" id=""></td>
										<th> হতে  : </th>
										<td><input type="date" class="bottom_border" name="" id=""></td> 
										<th>তারিখ  পর্যন্ত  । </th>
										<th>নীরিক্ষার মেয়াদকাল  :  </th>
										<td><input type="date" class="bottom_border" name="" id=""></td>
										<th>হতে : </th> 
										<td><input type="date" class="bottom_border"  name="" id=""></td>
										<th colspan="2">তারিখ  পর্যন্ত  ।</th>
									</tr>
									
								</tbody>
							</table>
						</div>

							</form>
					</div>
					
							
			

				</form>
		  </div>
			
			<!-- Form 1 End ############################################################-->
			
			<!-- Form 2 Start ############################################################-->
			
			
			
					<div class="tab"> Form 2

					  <form class="programForm" action="">
						<div class="table-responsive">
							<table>
								<thead>
									<tr>
										<td>ক্রঃ নং</td>
										<td colspan="2">প্রাপ্ত অনিয়ম/আপত্তি/ভুল-ত্রুটি</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>
											পূর্ববর্তী অডিট প্রতিবেদন ফলোআপ সংক্রান্ত
										</td>
										<td><textarea name="" id="" cols="100" rows="3"></textarea></td>
									</tr>

									<tr>
										<td>2</td>
										<td colspan="2">
											সমিতি পরিদর্শন সংক্রান্ত:<br>
											ক. সমিতি ও পাশবই যাচাইয়ে কর্মী ভিত্তিক প্রাপ্ত ভুল-ত্রুটিঃ  
											<div class="pull-right">
												<button type="button" id="add_row" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></button>
											</div>  
											<table id="data">
												<thead>
													<tr>
														<td rowspan="3" width="8%">কর্মীর নাম</td>
														<td colspan="16">সমিতি সংক্রান্ত বিভিন্ন তথ্য</td>
														<td colspan="4" rowspan="2">পাশবইয়ের ভুল-ত্রুটি সংক্রান্ত</td>
														<td  rowspan="3">পাশবই যাচাই করা হয়নি</td>
													</tr>
													<tr>
														<td rowspan="2">মোট সমিতির সংখ্যা</td>
														<td colspan="2">সমিতিতে সদস্যদের বসার পরিবেশ</td>
														<td colspan="5">সমিতির মিটিং এ সদস্য উপস্থিতি</td>
														<td colspan="5">সমিতির সঞ্চয় ও কিস্তি আদায় সংক্রান্ত</td>
														<td  rowspan="2">অনিয়মিত সঞ্চয় জমাকারী সদস্য সংখ্যা</td>
														<td  rowspan="2">রেজুলেশন খাতা নেই সংখ্যা</td>
														<td  rowspan="2">১৫ জনের নীচে সমিতির সংখ্যা</td>
														
													</tr>
													<tr>
														<td>আছে(সংখ্যা)</td>
														<td>নাই(সংখ্যা)</td>
														<td>মোট সদস্য</td>
														<td> নির্দিষ্ট সময়ে উপস্থিত</td>
														<td> বিলম্বে উপস্থিত</td>
														<td> মোট উপস্থিত</td>
														<td>উপস্থিতির হার</td>
														<td>সমিতি থেকে</td>
														<td>বাড়ি ঘর থেকে</td>
														<td>অন্যের দ্বারা</td>
														<td>দোকান/বাজার থেকে</td>
														<td>মোট আদায়</td>
														<td>যোগ বিয়োগের ভুল-ত্রুটি</td>
														<td>নমুনা স্বাক্ষর</td>
														<td>সদস্যদের ছবি</td>
														<td>পাশবইয়ের তথ্য আপডেট হয় না</td>
													</tr>
												</thead>

												<tbody>
													
													<tr>
														<td><input type="text" class="" style="width:80px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td><b>মোট</b></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
													</tr>
													<tr>
														<td colspan="5">সংযুক্তিঃ </td>
														<td colspan="17"><input type="file" name="" id=""> </td>
													</tr>
														<tr>
														<td colspan="2">খ)</td>
														<td colspan="20"><input type="text" name="" id=""> </td>
													</tr>
													<tr>
														<td colspan="2">গ) </td> 
														<td colspan="20"><input type="text" name="" id=""> </td>
													</tr>
												</tfoot>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

				</div>
					<!-- Form 2 end ############################################################-->
					
					
				  <div class="tab"> Form 2

				  <form class="programForm" action="">
			
					<div class="table-responsive">
					<form class="programForm" action="">
						<div class="table-responsive">
							<table>
								<thead>
									<tr>
										<td>ক্রঃ নং</td>
										<td colspan="2">প্রাপ্ত অনিয়ম/আপত্তি/ভুল-ত্রুটি</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>3</td>
										<td colspan="2">
											সদস্যদের সকল প্রকার পাশবই যাচাই সংক্রান্ত:
										</td>
									</tr>

									<tr>
										<td>2</td>
										<td colspan="2">
											সমিতি পরিদর্শন সংক্রান্ত:<br>
											ক. সমিতি ও পাশবই যাচাইয়ে কর্মী ভিত্তিক প্রাপ্ত ভুল-ত্রুটিঃ  
											<div class="pull-right">
												<button type="button" id="add_row" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></button>
											</div>  
											<table id="data">
												<thead>
													<tr>
														<td rowspan="3" width="8%">কর্মীর নাম</td>
														<td colspan="16">সমিতি সংক্রান্ত বিভিন্ন তথ্য</td>
														<td colspan="4" rowspan="2">পাশবইয়ের ভুল-ত্রুটি সংক্রান্ত</td>
														<td  rowspan="3">পাশবই যাচাই করা হয়নি</td>
													</tr>
													<tr>
														<td rowspan="2">মোট সমিতির সংখ্যা</td>
														<td colspan="2">সমিতিতে সদস্যদের বসার পরিবেশ</td>
														<td colspan="5">সমিতির মিটিং এ সদস্য উপস্থিতি</td>
														<td colspan="5">সমিতির সঞ্চয় ও কিস্তি আদায় সংক্রান্ত</td>
														<td  rowspan="2">অনিয়মিত সঞ্চয় জমাকারী সদস্য সংখ্যা</td>
														<td  rowspan="2">রেজুলেশন খাতা নেই সংখ্যা</td>
														<td  rowspan="2">১৫ জনের নীচে সমিতির সংখ্যা</td>
														
													</tr>
													<tr>
														<td>আছে(সংখ্যা)</td>
														<td>নাই(সংখ্যা)</td>
														<td>মোট সদস্য</td>
														<td> নির্দিষ্ট সময়ে উপস্থিত</td>
														<td> বিলম্বে উপস্থিত</td>
														<td> মোট উপস্থিত</td>
														<td>উপস্থিতির হার</td>
														<td>সমিতি থেকে</td>
														<td>বাড়ি ঘর থেকে</td>
														<td>অন্যের দ্বারা</td>
														<td>দোকান/বাজার থেকে</td>
														<td>মোট আদায়</td>
														<td>যোগ বিয়োগের ভুল-ত্রুটি</td>
														<td>নমুনা স্বাক্ষর</td>
														<td>সদস্যদের ছবি</td>
														<td>পাশবইয়ের তথ্য আপডেট হয় না</td>
													</tr>
												</thead>

												<tbody>
													<tr>
														<td><input type="text" name="" id="" class="" style="width:80px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>
													</tr>

													<tr>
														<td><input type="text" class="" style="width:80px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td><b>মোট</b></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
														<td><input type="text" class="only_numeric" style="width:50px;"></td>
													</tr>
													<tr>
														<td colspan="5">সংযুক্তিঃ </td>
														<td colspan="17"><input type="file" name="" id=""> </td>
													</tr>
														<tr>
														<td colspan="2">খ)</td>
														<td colspan="20"><input type="text" name="" id=""> </td>
													</tr>
													<tr>
														<td colspan="2">গ) </td> 
														<td colspan="20"><input type="text" name="" id=""> </td>
													</tr>
												</tfoot>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				  </div>
				 </div>
				  <div class="tab">Birthday:
					<p><input placeholder="dd" oninput="this.className = ''" name="dd"></p>
					<p><input placeholder="mm" oninput="this.className = ''" name="nn"></p>
					<p><input placeholder="yyyy" oninput="this.className = ''" name="yyyy"></p>
				  </div>
				  <div class="tab">Login Info:
					<p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
					<p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>
				  </div>
				  <br>
				  <div style="overflow:auto;">
					<div style="float:right;">
					  <button type="button" class="btn btn-xs btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
					  <button type="button" class="btn btn-xs btn-info" id="nextBtn" onclick="nextPrev(1)">Next</button>
					</div>
				  </div>
				  <!-- Circles which indicates the steps of the form: -->
				  <div style="text-align:center;margin-top:40px;">
					<span class="step"></span>
					<span class="step"></span>
					<span class="step"></span>
					<span class="step"></span>
				  </div>
		</form>
		
				</div>
				</div>
			</div>
		</div>
</section>

	</section>

	<script>
	$(document).ready(function() {
      $(".only_numeric").bind("keypress", function (e) {
          var keyCode = e.which ? e.which : e.keyCode
          if (!(keyCode >= 48 && keyCode <= 57)) {
            return false;
          }else{
			  return true
          }
      });
		$("#MainGroupAudit_Entry_Form").addClass('active');
		$("#Program").addClass('active');
    });
	var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementsByClassName("programForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
	
	$("#add_row").unbind('click').bind('click', function() {
		var table = $("#data");
		var count_table_tbody_tr = $("#data tbody tr").length;
		var row_id = count_table_tbody_tr + 1;
		var html = '<tr id="row_'+row_id+'">'+
				'<td><input type="text" name="" id="" class="" style="width:80px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:50px;"></td>'+
				'</tr>';

				if(count_table_tbody_tr >= 1) {
					$("#data tbody tr:last").after(html);  
				}else {
					$("#data tbody").html(html);
				}
				 $(".only_numeric").bind("keypress", function (e) {
					  var keyCode = e.which ? e.which : e.keyCode
					  if (!(keyCode >= 48 && keyCode <= 57)) {
						return false;
					  }else{
						  return true
					  }
				  });
		});

</script>

@endsection