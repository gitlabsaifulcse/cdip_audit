@extends('admin.admin_master')
@section('title', 'Program Form')
@section('main_content')

<br>
<br>
<br>
<style>
    table, tr, th, td {
    border: 1px  solid black;
    text-align:center;
    font-size:14px !important;
    }
    table{
        width:100%;
    }
</style>

<section class="content">
    <div class="row">
        <div class="col-md-12"> 
			<div class="box box-info" style="margin-bottom:10px;">
				<div class="box-body">
                    <div class="pull-right">
                        <button type="button" id="add_row_table1" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></button>
                    </div>  
                   <table id="table1">
                       <caption style="text-align: center;">সঞ্চয় ও ঋন কর্মসুচি সংক্রান্তঃ </caption>
                       <thead>
                           <tr>
                               <td colspan="10"> ক) প্রিন্ট কালেকশনশীটের পোস্টিং যাচাইয়ের প্রাপ্ত 'ঘাটতি' সংক্রান্তঃ</td>
                           </tr>
                            <tr>
                                <td rowspan="2">#</td>
                                <td rowspan="2">কর্মীর নাম</td>
                                <td rowspan="2">আইডি নং</td>
                                <td rowspan="2">কর্মীর বর্তমান অবস্থান</td>
                                <td colspan="2">সাধারন সঞ্চয়</td>
                                <td colspan="2">স্বেচ্ছা সঞ্চয়</td>
                                <td colspan="2">মাসিক সঞ্চয়</td>
                                <td colspan="2">ঋনের কিস্তি</td>
                                <td colspan="2">মোট</td>
                            </tr>
                            <tr>
                                <td>জন</td>
                                <td>টাকা</td>
                                <td>জন</td>
                                <td>টাকা</td>
                                <td>জন</td>
                                <td>টাকা</td>
                                <td>জন</td>
                                <td>টাকা</td>
                                <td>জন</td>
                                <td>টাকা</td>
                            </tr>
                       </thead>
                       <tbody>
                            <tr>
                                <td>1</td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                            </tr>

                       </tbody>
                       <tfoot>
                           <tr>
                               <td colspan="12"><input type="file"></td>
                               <td colspan="2">(সংযুক্তি ..........)</td>
                           </tr>

                            <tr>
                                <td colspan="2">খ)</td>
                                <td  colspan="4"><input type="text" style="width:100%;"></td>
                                <td colspan="2">সংক্রান্তঃ</td>
                                <td  colspan="6"><input type="text" style="width:100%;"></td>
                            </tr>
                            <tr>
                                <td colspan="2">গ)</td>
                                <td  colspan="4"><input type="text" style="width:100%;"></td>
                                <td colspan="2">সংক্রান্তঃ</td>
                                <td  colspan="6"><input type="text" style="width:100%;"></td>
                            </tr>
                       </tfoot>
                   </table>
                </div>

                <div class="box-body">
                    <div class="pull-right">
                        <button type="button" id="add_row_table2" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></button>
                    </div>  
                   <table id="table2">
                      
                       <thead>
                           <tr>
                               <td colspan="11"> ঘ) মাসিক কিস্তি আদায় করে 'নিদিষ্ট সময়ের পর' অফিসে জমা করা সংক্রান্তঃ</td>
                           </tr>
                            <tr>
                                <td rowspan="2">#</td>
                                <td rowspan="2">সদস্যদের নাম</td>
                                <td rowspan="2">স্বামীর নাম</td>
                                <td rowspan="2">সমিতির নাম</td>
                                <td rowspan="2">কর্মীর নাম</td>

                                <td colspan="2">পাশবইয়ে আদায় </td>
                                <td colspan="2">অফিসে জমা</td>
                                <td colspan="2">হাতে রেখে 'পরে' জমা (দিন)</td>
                            </tr>
                            <tr>
                                <td>জন</td>
                                <td>টাকা</td>

                                <td>জন</td>
                                <td>টাকা</td>

                                <td>জন</td>
                                <td>টাকা</td>
                             
                            </tr>
                       </thead>
                       <tbody>
                            <tr>
                                <td>1</td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                            </tr>

                       </tbody>
                       <tfoot>
                           <tr>
                               <td colspan="8"><input type="file"></td>
                               <td colspan="3">(সংযুক্তি ..........)</td>
                           </tr>
                       </tfoot>
                   </table>
                </div>



                <div class="box-body">
                    <div class="pull-right">
                        <button type="button" id="add_row_table3" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></button>
                    </div>  
                   <table id="table3">
                      
                       <thead>
                           <tr>
                               <td colspan="11"> ঙ) মাসিক কিস্তি আদায়ের 'পূর্বে' অফিসে জমা করা সংক্রান্তঃ</td>
                           </tr>
                            <tr>
                                <td rowspan="2">#</td>
                                <td rowspan="2">সদস্যদের নাম</td>
                                <td rowspan="2">স্বামীর নাম</td>
                                <td rowspan="2">সমিতির নাম</td>
                                <td rowspan="2">কর্মীর নাম</td>

                                <td colspan="2">পাশবইয়ে আদায় </td>
                                <td colspan="2">অফিসে জমা</td>
                                <td colspan="2">হাতে রেখে 'পূর্বে' জমা (দিন)</td>
                            </tr>
                            <tr>
                                <td>জন</td>
                                <td>টাকা</td>

                                <td>জন</td>
                                <td>টাকা</td>

                                <td>জন</td>
                                <td>টাকা</td>
                             
                            </tr>
                       </thead>
                       <tbody>
                            <tr>
                                <td>1</td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                                <td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>
                            </tr>

                       </tbody>
                       <tfoot>
                        <tr>
                            <td colspan="2">চ)</td>
                            <td  colspan="3"><input type="text" style="width:100%;"></td>
                            <td colspan="2">সংক্রান্তঃ</td>
                            <td  colspan="4"><input type="text" style="width:100%;"></td>
                        </tr>

                           
                       </tfoot>
                   </table>
                </div>

            </div>
        </div>
    </div>
    
    
</section>



<script>
    $(document).ready(function() {
        $("#MainGroupAudit_Entry_Form").addClass('active');
        $("#Deposit_and_Loan").addClass('active');
    });
    /* Table 1 */
    $("#add_row_table1").unbind('click').bind('click', function() {
		var table = $("#table1");
		var count_table_tbody_tr = $("#table1 tbody tr").length;
		var row_id = count_table_tbody_tr + 1;
		var html = '<tr id="row_'+row_id+'">'+
				'<td>'+row_id+'</td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				
				'</tr>';

				if(count_table_tbody_tr >= 1) {
					$("#table1 tbody tr:last").after(html);  
				}else {
					$("#table1 tbody").html(html);
				}
				 $(".only_numeric").bind("keypress", function (e) {
					  var keyCode = e.which ? e.which : e.keyCode
					  if (!(keyCode >= 48 && keyCode <= 57)) {
						return false;
					  }else{
						  return true
					  }
				  });
		});


        /* Table 2 */
        $("#add_row_table2").unbind('click').bind('click', function() {
		var table = $("#table2");
		var count_table_tbody_tr = $("#table2 tbody tr").length;
		var row_id = count_table_tbody_tr + 1;
		var html = '<tr id="row_'+row_id+'">'+
				'<td>'+row_id+'</td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				
				'</tr>';

				if(count_table_tbody_tr >= 1) {
					$("#table2 tbody tr:last").after(html);  
				}else {
					$("#table2 tbody").html(html);
				}
				 $(".only_numeric").bind("keypress", function (e) {
					  var keyCode = e.which ? e.which : e.keyCode
					  if (!(keyCode >= 48 && keyCode <= 57)) {
						return false;
					  }else{
						  return true
					  }
				  });
		});

          /* Table 3 */
        $("#add_row_table3").unbind('click').bind('click', function() {
		var table = $("#table3");
		var count_table_tbody_tr = $("#table3 tbody tr").length;
		var row_id = count_table_tbody_tr + 1;
		var html = '<tr id="row_'+row_id+'">'+
				'<td>'+row_id+'</td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
                    '<td><input type="text" name="" id="" class="only_numeric" style="width:100%;"></td>'+
				'</tr>';

				if(count_table_tbody_tr >= 1) {
					$("#table3 tbody tr:last").after(html);  
				}else {
					$("#table3 tbody").html(html);
				}
				 $(".only_numeric").bind("keypress", function (e) {
					  var keyCode = e.which ? e.which : e.keyCode
					  if (!(keyCode >= 48 && keyCode <= 57)) {
						return false;
					  }else{
						  return true
					  }
				  });
		});

</script>

@endsection