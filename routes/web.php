<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

use App\Http\Controllers\AdminController;

Route::get('/admin', [AdminController::class, 'index']);
Route::post('admin-login-check', [AdminController::class, 'adminLoginCheck']);

use App\Http\Controllers\SuperAdminController;
Route::get('dashboard', [SuperAdminController::class, 'index']);

// NAV Group Manager
use App\Http\Controllers\MenuGroupController;

Route::get('/manage-menu-group', [MenuGroupController::class, 'index']);
Route::get('/add_group_menu', [MenuGroupController::class, 'add_menu_group']); 
Route::post('/store-menu-group', [MenuGroupController::class, 'stote_menu_group']); 
Route::get('/edit-nav-group/{nav_group_id}', [MenuGroupController::class, 'edit_nav_group']); 
Route::post('/update-menu-group', [MenuGroupController::class, 'update_nav_group']); 
 
 use App\Http\Controllers\MenusController;

Route::get('/manage-menu', [MenusController::class, 'index']);
Route::get('/add_menu', [MenusController::class, 'add_menu']);
Route::POST('/store-menu', [MenusController::class, 'stote_menu']);
Route::get('/edit-nav/{nav_id}', [MenusController::class, 'edit_nav']);
Route::get('/update-menu', [MenusController::class, 'update_nav']);
//NAV Manager    


//User Manager
use App\Http\Controllers\AdminUserController;
Route::get('/manage-user', [AdminUserController::class, 'index']);
Route::get('/add_user', [AdminUserController::class, 'add_user']);
Route::POST('/store-user', [AdminUserController::class, 'stote_user']);
Route::POST('/update-user', [AdminUserController::class, 'update_user']);
Route::get('/edit-user/{admin_id}', [AdminUserController::class, 'edit_user']);
Route::get('/edit-user/{admin_id}', [AdminUserController::class, 'edit_user']);
Route::get('/edit-user/{admin_id}', [AdminUserController::class, 'edit_user']);

Route::get('/delete-user/{admin_id}','AdminUserController@destroy_user');

Route::get('/paward_change','AdminUserController@paward_change');
Route::get('/check_password_old/{old_password}','AdminUserController@check_password_old');
Route::POST('/new_password_insert','AdminUserController@new_password_insert');


Route::get('/ad_reset_pw','AdminUserController@admin_reset_password');
Route::get('/update_reset_password/{admin_id}','AdminUserController@update_reset_password');
//
use App\Http\Controllers\ProgramFormController;
Route::get('/program_form', [ProgramFormController::class, 'index']);
Route::POST('/audit_submission_form', [ProgramFormController::class, 'audit_submission_form']);
Route::get('/program_form/pass_book_check', [ProgramFormController::class, 'pass_book_check']);
Route::get('/program_form/deposit_and_loan', [ProgramFormController::class, 'deposit_and_loan']);